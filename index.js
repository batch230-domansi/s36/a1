/*
	Preparations:
	npm init -y
	npm install express
	npm install mongoose
*/


// require the installed modules
const express = require("express");
const mongoose = require("mongoose");

// port
const port = 3001;

// server
const app = express();

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.yat49rm.mongodb.net/s36?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error" ));

db.once("open", () => console.log("We're connected to the cloud database."));

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Routes Grouping - organize the access for each resources.
const taskRoutes = require("./routes/taskRoutes");
app.use("/tasks", taskRoutes); //localhost:4000/tasks/


// port listener
app.listen(port, () => console.log(`Server is running at port ${port}`))