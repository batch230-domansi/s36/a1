const Task = require("../models/Task");

// function createTaskController(req, res){ //statements } << export this funtion
module.exports.createTaskController = (req, res) => {
	// checking captured data from the request body.
	console.log(req.body);

	Task.findOne({name: req.body.name}).then(result =>
	{
		console.log(result);

		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found")
		}
		else{
			let newTask = new Task({
				name: req.body.name,
				status: req.body.status
			})

			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error))
};

// Retrieve ALL Tasks
module.exports.getAllTasksController = (req, res) => {
	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}

// Deleting a task
module.exports.deleteTaskController = (req, res) => {
	console.log(req.params.taskId)

	Task.findByIdAndRemove(req.params.taskId)
	.then(result => res.send(result))
	.catch(error => res.send(error));
}


module.exports.getSpecificTaskName =(req,res) =>{
	console.log(req.params.id);
	Task.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
}
module.exports.changeStatustoName = (taskId) => {

	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	// The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function 
	return Task.findById(taskId).then((result, err) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(err){

			console.log(err);
			return err;

		}

		// Change the status of the returned document to "complete"
		result.status = "name";

		// Saves the updated object in the MongoDB database
		// The document already exists in the database and was stored in the "result" parameter, we can use the "save" method to update the existing document with the changes we applied
		// The "return" statement returns the result of the "save" method to the "then" method chained to the "findById" method  which invokes this function
		return result.save().then((updatedTask, saveErr) => {

			// If an error is encountered returns a "false" boolean back to the client/Postman
			if (saveErr) {

				console.log(saveErr);
				// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
				return saveErr;

			// Update successful, returns the updated task object back to the client/Postman
			} else {

				// The "return" statement returns the result to the  "then" method chained to the "save" method which invokes this function
				return updatedTask;

			}
		})
	})

}

module.exports.updatedTaskNameController =(req,res) => {

	console.log(req.params.taskId);
	console.log(req.body);

	let updates ={
		name : req.body.name
	}
  Task.findByIdAndUpdate(re.params.taskId, updates, {new : true})
  .then(updatedTask => res.send(updatedTask))
  .catch(error => res.send(error));

}





// -- activity 
// Update Specific Task through URL
module.exports.changeStatustoComplete = (taskId) => {

	return Task.findById(taskId).then((result, err) => {
		if(err){

			console.log(err);
			return err;

		}
		result.status = "complete";
		return result.save().then((updatedTask, saveErr) => {

			
			if (saveErr) {

				console.log(saveErr);
				
				return saveErr;

			} else {

				
				return updatedTask;

			}
		})
	})

}

module.exports.getSpecificTaskID = (req, res) => {
	console.log(req.params.taskId);
	Task.findById(req.params.taskId)
	.then(result => res.send(result))
	.catch(error => res.send(error));
}